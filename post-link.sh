# FSL >= 6.0.6
if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper fsl_sub fsl_sub_config fsl_sub_plugin fsl_sub_report fsl_sub_update

# FSL <= 6.0.5
elif [ -e "${FSLDIR}/etc/fslconf/requestFSLpythonLink.sh" ]; then
    "$FSLDIR/etc/fslconf/requestFSLpythonLink.sh" fsl_sub fsl_sub_config fsl_sub_plugin fsl_sub_report fsl_sub_update
fi

